package acme

const (
	LetsencryptStaging    = "https://acme-staging-v02.api.letsencrypt.org/directory"
	LetsencryptProduction = "https://acme-v02.api.letsencrypt.org/directory"
	ZeroSSLProduction     = "https://acme.zerossl.com/v2/DV90"
)
